#Profiles

#####Table of Contents
1. [Overview](#overview)
2. [Setup - The basics of getting started with apache](#setup)
    * [What apache affects](#what-apache-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with apache](#beginning-with-apache)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

##Overview
Each profile class manages a specific part of the infrastructure. 

###Module Description
All Hiera lookups should be performed from here. Everything related to getting a certain
piece of the infrastructure configured should be placed in a profile.

An example setup for an Apache profile would include the following:

* Include Apache
* Firewall rules needed (if any)
* logrotate.d config file
* Virtual hosts

##Reference

##Usage
When using the `include` function when creating your profiles you will need to append `::` when declaring classes.

Example for including the apache module:

```puppet
include ::apache
```

This will not work because it will be looking for a profile with the class of 'apache'.

```puppet
include apache
```

###Classes

* apache.pp
* base.pp
* nginx.pp
* nrpe.pp
* php.pp
* snmpd.pp
* supervisord.pp

###Contributors

Mike Travis - <mtravis@webcourseworks.com>