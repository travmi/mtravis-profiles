# Class profile::ahip::apache
#
#
class profile::ahip::apache {
  
  include ::apache
  
  apache::vhost { 'aetna.cmpsystem.com':
    url                  => 'aetna.cmpsystem.com',
    server_name          => 'aetna.cmpsystem.com',
    client_folder        => aetna,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,
    server_alias         => [ 'aetna' ],
    error_log            => aetna_cmpsystem_com_error_log,
    access_log           => aetna_cmpsystem_com_apache_access_log
  }

  apache::vhost { '_ahipmedicaretraining.com':
    url                  => 'ahipmedicaretraining.com',
    server_name          => 'ahipmedicaretraining.com',
    client_folder        => ahip,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,
    server_alias         => [ 'ahipmedicare.com',
                              'www.ahipmedicare.com',
                              'ahipmedicare.org',
                              'www.ahipmedicare.org',
                              'ahipmedicarecourse.com',
                              'www.ahipmedicarecourse.com',
                              'ahipmedicarecourse.org',
                              'www.ahipmedicarecourse.org',
                              'ahipmedicaretraining.com',
                              'ahipmedicaretraining.org',
                              'www.ahipmedicaretraining.org',
                              'ahip.webcourseworks.com',
                              'ciepd.webcourseworks.com' ],
    error_log            => ahipmedicaretraining_com_error_log,
    access_log           => ahipmedicaretraining_com_access_log
  }  

  apache::vhost { 'clearstone.cmpsystem.com':
    url                  => 'clearstone.cmpsystem.com',
    server_name          => 'clearstone.cmpsystem.com',
    client_folder        => clearstone,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,
    server_alias         => [ 'clearstone' ],
    error_log            => clearstone_cmpsystem_com_error_log,
    access_log           => clearstone_cmpsystem_com_access_log
  }

  apache::vhost { 'coventry.cmpsystem.com':
    url                  => 'coventry.cmpsystem.com',
    server_name          => 'coventry.cmpsystem.com',
    client_folder        => coventry,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,
    server_alias         => [ 'coventry' ],
    error_log            => coventry_cmpsystem_com_error_log,
    access_log           => coventry_cmpsystem_com_apache_access_log
  }

  apache::vhost { 'demo.ahipmedicaretraining.com':
    url                  => 'demo.ahipmedicaretraining.com',
    server_name          => 'demo.ahipmedicaretraining.com',
    client_folder        => demo,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,   
    server_alias         => [ 'demo' ],
    error_log            => demo_ahipmedicaretraining_com_error_log,
    access_log           => demo_ahipmedicaretraining_com_apache_access_log
  }

  apache::vhost { 'healthnet.cmpsystem.com':
    url                  => 'healthnet.cmpsystem.com',
    server_name          => 'healthnet.cmpsystem.com',
    client_folder        => healthnet,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,   
    server_alias         => [ 'healthnet' ],
    error_log            => healthnet_cmpsystem_com_error_log,
    access_log           => healthnet_cmpsystem_com_apache_access_log
  }

  apache::vhost { 'healthspring-archive.cmpsystem.com':
    url                  => 'healthspring-archive.cmpsystem.com',
    server_name          => 'healthspring-archive.cmpsystem.com',
    client_folder        => healthspring,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,    
    server_alias         => [ 'healthspring-archive' ],
    error_log            => healthspring-archive_cmpsystem_com_error_log,
    access_log           => healthspring-archive_cmpsystem_com_apache_access_log
  }

  apache::vhost { 'healthspring.cmpsystem.com':
    url                  => 'healthspring.cmpsystem.com',
    server_name          => 'healthspring.cmpsystem.com',
    client_folder        => healthspring,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,   
    server_alias         => [ 'healthspring' ],
    error_log            => healthspring_cmpsystem_com_error_log,
    access_log           => healthspring_cmpsystem_com_apache_access_log
  }

  apache::vhost { 'humana.coursestage.com':
    url                  => 'humana.coursestage.com',
    server_name          => 'humana.coursestage.com',
    client_folder        => humana,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,   
    server_alias         => [ 'humana.coursestage.com' ],
    error_log            => humana_coursestage_com_error_log,
    access_log           => humana_coursestage_com_apache_access_log
  }

  apache::vhost { 'scan.cmpsystem.com':
    url                  => 'scan.cmpsystem.com',
    server_name          => 'scan.cmpsystem.com',
    client_folder        => scan,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,   
    server_alias         => [ 'scan' ],
    error_log            => scan_cmpsystem_com_error_log,
    access_log           => scan_cmpsystem_com_apache_access_log
  }

  apache::vhost { 'wellpoint.cmpsystem.com':
    url                  => 'wellpoint.cmpsystem.com',
    server_name          => 'wellpoint.cmpsystem.com',
    client_folder        => wellpoint,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,    
    server_alias         => [ 'wellpoint' ],
    error_log            => wellpoint_cmpsystem_com_error_log,
    access_log           => wellpoint_cmpsystem_com_apache_access_log
  }

  apache::vhost { 'wellpointset.cmpsystem.com':
    url                  => 'wellpointset.cmpsystem.com',
    server_name          => 'wellpointset.cmpsystem.com',
    client_folder        => wellpoint,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true, 
    server_alias         => [ 'wellpointset' ],
    error_log            => wellpointset_cmpsystem_com_error_log,
    access_log           => wellpointset_cmpsystem_com_apache_access_log
  }

  apache::vhost { 'www.ahipexchangetraining.com':
    url                  => 'www.ahipexchangetraining.com',
    server_name          => 'www.ahipexchangetraining.com',
    client_folder        => aca,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,    
    server_alias         => [ 'ahipexchangetraining.com' ],
    error_log            => www_ahipexchangetraining_com_error_log,
    access_log           => www_ahipexchangetraining_com_access_log
  }

  apache::vhost { 'www.ahipmedicaretraining.com':
    url                  => 'ahip.webcourseworks.com',
    server_name          => 'www.ahipmedicaretraining.com',
    client_folder        => ahip,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,  
    server_alias         => [ 'ahipmedicaretraining' ],
    error_log            => ahip_webcourseworks_com_error_log,
    access_log           => ahip_webcourseworks_com_apache_access_log
  }

  apache::vhost { '_www.cmpsystem.com':
    url                  => 'www.cmpsystem.com',
    server_name          => 'www.cmpsystem.com',
    client_folder        => ahip,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,  
    server_alias         => [ 'cmpsystem.com',
                              'www.cmpsystem.net',
                              'cmpsystem.net',
                              'www.cmpsystem.org',
                              'cmpsystem.org' ],
    error_log            => www_cmpsystem_com_error_log,
    access_log           => www_cmpsystem_com_access_log
  }

  apache::vhost { 'xyzhealth.cmpsystem.com':
    url                  => 'xyzhealth.cmpsystem.com',
    server_name          => 'xyzhealth.cmpsystem.com',
    client_folder        => demo,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true, 
    server_alias         => [ 'xyzhealth' ],
    error_log            => xyzhealth_cmpsystem_com_error_log,
    access_log           => xyzhealth_cmpsystem_com_apache_access_log
  }

  apache::vhost { 'xyzhealthset.cmpsystem.com':
    url                  => 'xyzhealthset.cmpsystem.com',
    server_name          => 'xyzhealthset.cmpsystem.com',
    client_folder        => demo,
    logstash_log_forward => true,
    error_doc            => true,
    options              => true,
    server_alias         => [ 'xyzhealthset' ],
    error_log            => xyzhealthset_cmpsystem_com_error_log,
    access_log           => xyzhealthset_cmpsystem_com_apache_access_log
  }

}