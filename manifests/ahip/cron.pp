class profile::ahip::cron {
  
  cron::job { 'clear_tmp':
    minute      => '*',
    hour        => '*/3',
    date        => '*',
    month       => '*',
    weekday     => '*',
    user        => 'root',
    command     => '/root/scripts/clear_tmp.sh'
  }

  cron::job { 'rsync':
    minute      => '*',
    hour        => '*/3',
    date        => '*',
    month       => '*',
    weekday     => '*',
    user        => 'root',
    command     => '/root/scripts/rsync.sh'
  }

}