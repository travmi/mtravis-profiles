class profile::ahip::hosts {

  host { 'ciepd-ecommerce.webcourseworks.com':
    ensure   => 'present',
    ip       => '192.168.11.161',
    provider => augeas
  }

  host { 'purchase.ahipmedicaretraining.com':
    ensure   => 'present',
    ip       => '192.168.11.161',
    provider => augeas
  }
  
  host { 'demopurchase.ahipmedicaretraining.com':
    ensure   => 'present',
    ip       => '192.168.11.161',
    provider => augeas
  }
  
  host { 'ciepd.webcourseworks.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'www.ahipmedicaretraining.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }
  
  host { 'humana.coursestage.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'humana.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'aetna.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'wellpoint.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'healthspring.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'coventry.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'healthnet.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'clearstone.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'bcbsaz.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'bcbsnpa.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'scan.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'wellpointset.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'xyzhealth.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'xyzhealthset.cmpsystem.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  host { 'demo.ahipmedicaretraining.com':
    ensure   => 'present',
    ip       => "${::ipaddress_eth1}",
    provider => augeas
  }

  
}