class profile::ahip::mounts {

  mounttab { '/mnt/nfs':
    ensure   => present,
    device   => 'nfs-ahip.wcw.local:/exports/ahip',
    fstype   => 'nfs',
    options  => [ 'intr',
                  'soft',
                  'async',
                  'tcp',
                  'nfsvers=3',
                  'noatime',
                  '_netdev' ],
    dump     => '0',
    pass     => '0',
    provider => augeas,
  }

  mounttab { '/var/www':
    ensure   => present,
    device   => '/mnt/nfs/home',
    fstype   => 'none',
    options  => [ 'bind',
                  '_netdev' ],
    dump     => '0',
    pass     => '0',
    provider => augeas,
    before   => Service['httpd', 'nginx']
  }

  # Need to verify all mounts are created before packages and httpd/nginx start.
  # Need to create mount point directories before mounting.
  Package['ruby-augeas'] -> Augeas <| |>

}