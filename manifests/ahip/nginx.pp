class profile::ahip::nginx {

  include ::nginx
 
  nginx::vhost { 'aetna.cmpsystem.com':
    client_folder => aetna,
    access_log    => aetna_cmpsystem_com_access_log,
    error_log     => aetna_cmpsystem_com_error_log,
    url           => 'aetna.cmpsystem.com'
  }

  nginx::vhost { 'coventry.cmpsystem.com':
    client_folder => coventry,
    access_log    => coventry_cmpsystem_com_access_log,
    error_log     => coventry_cmpsystem_com_error_log,
    url           => 'coventry.cmpsystem.com'
  }
  
  nginx::vhost { 'demo.ahipmedicaretraining.com':
    client_folder => demo,
    access_log    => demo_ahipmedicaretraining_com_access_log,
    error_log     => demo_ahipmedicaretraining_com_error_log,
    url           => 'demo.ahipmedicaretraining.com'
  }
  
  nginx::vhost { 'healthnet.cmpsystem.com':
    client_folder => healthnet,
    access_log    => healthnet_cmpsystem_com_access_log,
    error_log     => healthnet_cmpsystem_com_error_log,
    url           => 'healthnet.cmpsystem.com'
  }

  nginx::vhost { 'healthspring.cmpsystem.com':
    client_folder => healthspring,
    access_log    => healthspring_cmpsystem_com_access_log,
    error_log     => healthspring_cmpsystem_com_error_log,
    url           => 'healthspring.cmpsystem.com'
  }
  
  nginx::vhost { 'humana.cmpsystem.com':
    client_folder => humana,
    access_log    => humana_coursestage_com_access_log,
    error_log     => humana_coursestage_com_error_log,
    url           => 'humana.cmpsystem.com'
  }
  
  nginx::vhost { 'scan.cmpsystem.com':
    client_folder => scan,
    access_log    => scan_cmpsystem_com_access_log,
    error_log     => scan_cmpsystem_com_error_log,
    url           => 'scan.cmpsystem.com'
  }
  
  nginx::vhost { 'wellpoint.cmpsystem.com':
    client_folder => wellpoint,
    access_log    => wellpoint_cmpsystem_com_access_log,
    error_log     => wellpoint_cmpsystem_com_error_log,
    url           => 'wellpoint.cmpsystem.com'
  }
  
  nginx::vhost { 'www.ahipexchangetraining.com':
    port          => 999,
    client_folder => aca,
    access_log    => ahip_exchangetraining_com_access_log,
    error_log     => ahip_exchangetraining_com_error_log,
    url           => 'www.ahipexchangetraining.com'
  }

  nginx::vhost { 'www.ahipmedicaretraining.com':
    client_folder => ahip,
    access_log    => ahip_webcourseworks_com_access_log,
    error_log     => ahip_webcourseworks_com_error_log,
    url           => 'www.ahipmedicaretraining.com'
  }

  nginx::vhost { 'xyzhealth.cmpsystem.com':
    client_folder => demo,
    access_log    => xyzhealth_cmpsystem_com_access_log,
    error_log     => xyzhealth_cmpsystem_com_error_log,
    url           => 'xyzhealth.cmpsystem.com'
  }
  
}