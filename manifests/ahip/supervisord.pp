class profile::ahip::supervisord {
  
  class { '::supervisord':
    install_pip  => true,
    install_init => true,
    nocleanup    => true,
  }

  supervisord::program { 'aetna':
    command                 => '/usr/bin/php /var/www/aetna/aetna.cmpsystem.com/htdocs/ext/transcript/store/cron.php',
    directory               => '/var/www/aetna/aetna.cmpsystem.com/htdocs/ext/transcript/store/',    
    numprocs                => '1',
    autostart               => true,
    autorestart             => 'true', # This is being validated with 'validate_re' and not 'validate_bool'
    startsecs               => '0',
    exitcodes               => '0,2',
    stdout_logfile          => 'aetna.log',
    stdout_logfile_maxbytes => '50MB',
    stdout_logfile_backups  => '10',
    stdout_capture_maxbytes => '0',
    stdout_events_enabled   => false,
    stderr_logfile          => 'aetna.error',
    stderr_logfile_maxbytes => '50MB',
    stderr_logfile_backups  => '10',
    stderr_capture_maxbytes => '0',
    stderr_events_enabled   => false,
  }

  supervisord::program { 'healthnet':
    command                 => '/usr/bin/php /var/www/healthnet/healthnet.cmpsystem.com/htdocs/ext/transcript/store/cron.php',
    directory               => '/var/www/healthnet/healthnet.cmpsystem.com/htdocs/ext/transcript/store/',    
    numprocs                => '1',
    autostart               => true,
    autorestart             => 'true', # This is being validated with 'validate_re' and not 'validate_bool'
    startsecs               => '0',
    exitcodes               => '0,2',
    stdout_logfile          => 'healthnet.log',
    stdout_logfile_maxbytes => '50MB',
    stdout_logfile_backups  => '10',
    stdout_capture_maxbytes => '0',
    stdout_events_enabled   => false,
    stderr_logfile          => 'healthnet.error',
    stderr_logfile_maxbytes => '50MB',
    stderr_logfile_backups  => '10',
    stderr_capture_maxbytes => '0',
    stderr_events_enabled   => false,
  }
  
  supervisord::program { 'humana':
    command                 => '/usr/bin/php /var/www/humana/humana.coursestage.com/htdocs/ext/transcript/store/cron.php',
    directory               => '/var/www/humana/humana.coursestage.com/htdocs/ext/transcript/store/',    
    numprocs                => '1',
    autostart               => true,
    autorestart             => 'true', # This is being validated with 'validate_re' and not 'validate_bool'
    startsecs               => '0',
    exitcodes               => '0,2',
    stdout_logfile          => 'humana.log',
    stdout_logfile_maxbytes => '50MB',
    stdout_logfile_backups  => '10',
    stdout_capture_maxbytes => '0',
    stdout_events_enabled   => false,
    stderr_logfile          => 'humana.error',
    stderr_logfile_maxbytes => '50MB',
    stderr_logfile_backups  => '10',
    stderr_capture_maxbytes => '0',
    stderr_events_enabled   => false,
  }

  supervisord::program { 'scan':
    command                 => '/usr/bin/php /var/www/scan/scan.cmpsystem.com/htdocs/ext/transcript/store/cron.php',
    directory               => '/var/www/scan/scan.cmpsystem.com/htdocs/ext/transcript/store/',    
    numprocs                => '1',
    autostart               => true,
    autorestart             => 'true', # This is being validated with 'validate_re' and not 'validate_bool'
    startsecs               => '0',
    exitcodes               => '0,2',
    stdout_logfile          => 'scan.log',
    stdout_logfile_maxbytes => '50MB',
    stdout_logfile_backups  => '10',
    stdout_capture_maxbytes => '0',
    stdout_events_enabled   => false,
    stderr_logfile          => 'scan.error',
    stderr_logfile_maxbytes => '50MB',
    stderr_logfile_backups  => '10',
    stderr_capture_maxbytes => '0',
    stderr_events_enabled   => false,
  }

  supervisord::program { 'wellpoint':
    command                 => '/usr/bin/php /var/www/wellpoint/wellpoint.cmpsystem.com/htdocs/ext/transcript/store/cron.php',
    directory               => '/var/www/wellpoint/wellpoint.cmpsystem.com/htdocs/ext/transcript/store/',    
    numprocs                => '1',
    autostart               => true,
    autorestart             => 'true', # This is being validated with 'validate_re' and not 'validate_bool'
    startsecs               => '0',
    exitcodes               => '0,2',
    stdout_logfile          => 'wellpoint.log',
    stdout_logfile_maxbytes => '50MB',
    stdout_logfile_backups  => '10',
    stdout_capture_maxbytes => '0',
    stdout_events_enabled   => false,
    stderr_logfile          => 'wellpoint.error',
    stderr_logfile_maxbytes => '50MB',
    stderr_logfile_backups  => '10',
    stderr_capture_maxbytes => '0',
    stderr_events_enabled   => false,
  }

  supervisord::program { 'ahip':
    command                 => '/usr/bin/php /var/www/ahip/ahip.webcourseworks.com/htdocs/ext/transcript/store/cron.php',
    directory               => '/var/www/ahip/ahip.webcourseworks.com/htdocs/ext/transcript/store/',    
    numprocs                => '1',
    autostart               => true,
    autorestart             => 'true', # This is being validated with 'validate_re' and not 'validate_bool'
    startsecs               => '0',
    exitcodes               => '0,2',
    stdout_logfile          => 'ahip.log',
    stdout_logfile_maxbytes => '50MB',
    stdout_logfile_backups  => '10',
    stdout_capture_maxbytes => '0',
    stdout_events_enabled   => false,
    stderr_logfile          => 'ahip.error',
    stderr_logfile_maxbytes => '50MB',
    stderr_logfile_backups  => '10',
    stderr_capture_maxbytes => '0',
    stderr_events_enabled   => false,
  }

  supervisord::program { 'ahipdemo':
    command                 => '/usr/bin/php /var/www/demo/demo.ahipmedicaretraining.com/htdocs/ext/transcript/store/cron.php',
    directory               => '/var/www/demo/demo.ahipmedicaretraining.com/htdocs/ext/transcript/store/',    
    numprocs                => '1',
    autostart               => true,
    autorestart             => 'true', # This is being validated with 'validate_re' and not 'validate_bool'
    startsecs               => '0',
    exitcodes               => '0,2',
    stdout_logfile          => 'ahipdemo.log',
    stdout_logfile_maxbytes => '50MB',
    stdout_logfile_backups  => '10',
    stdout_capture_maxbytes => '0',
    stdout_events_enabled   => false,
    stderr_logfile          => 'ahipdemo.error',
    stderr_logfile_maxbytes => '50MB',
    stderr_logfile_backups  => '10',
    stderr_capture_maxbytes => '0',
    stderr_events_enabled   => false,
  }

  supervisord::program { 'xyzhealth':
    command                 => '/usr/bin/php /var/www/demo/xyzhealth.cmpsystem.com/htdocs/ext/transcript/store/cron.php',
    directory               => '/var/www/demo/xyzhealth.cmpsystem.com/htdocs/ext/transcript/store/',    
    numprocs                => '1',
    autostart               => true,
    autorestart             => 'true', # This is being validated with 'validate_re' and not 'validate_bool'
    startsecs               => '0',
    exitcodes               => '0,2',
    stdout_logfile          => 'xyzhealth.log',
    stdout_logfile_maxbytes => '50MB',
    stdout_logfile_backups  => '10',
    stdout_capture_maxbytes => '0',
    stdout_events_enabled   => false,
    stderr_logfile          => 'xyzhealth.error',
    stderr_logfile_maxbytes => '50MB',
    stderr_logfile_backups  => '10',
    stderr_capture_maxbytes => '0',
    stderr_events_enabled   => false,
  }

}