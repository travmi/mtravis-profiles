# class: profile::base
#
#
class profile::base {

  include ::repo
  include ::snmpd
  include ::vim
  include ::nrpe
  include ::misc
  include ::postfix
  include ::dnsclient
  include ::augeas
    
  firewall { '200 allow nrpe access':
    port   => 5666,
    proto  => 'tcp',
    action => accept
  }

  firewall { '300 allow snmpd access':
    port   => [161,162],
    proto  => 'udp',
    action => accept,
    source => '10.1.25.120'
  }
    
  firewall { '400 allow ssh access':
    port   => 22,
    proto  => 'tcp',
    action => accept
  }    

  logrotate::file { 'awesant':
    log        => [ '/var/log/awesant/*.log' ],
    options    => [ 'daily',
                    'notifemty',
                    'rotate 7',
                    'compress',
                    'delaycompress' ],
    postrotate => [ '' ],         
  }

  logrotate::file { 'syslog':
    log        => [ '/var/log/messages', '/var/log/secure', '/var/log/maillog', '/var/log/spooler', '/var/log/boot.log', '/var/log/cron' ],
    options    => [ 'missingok',
                    'daily',
                    'notifemty',
                    'rotate 10',
                    'compressoptions -9',
                    'compressext .bz2',
                    'compress',
                    'delaycompress',
                    'create 600' ],
    postrotate => [ '/bin/kill -HUP `cat /var/run/syslogd.pid 2> /dev/null` 2> /dev/null || true',
                    '/bin/kill -HUP `cat /var/run/rsyslogd.pid 2> /dev/null` 2> /dev/null || true' ]
  }

#  nrpe_command { "check_spec_test":
#    ensure  => present,
#    command => "/usr/bin/check_my_thing -p 'some command with \"multiple [types]\" of quotes' -x and-stuff",
#  }
  
  class { 'selinux':
    mode => 'disabled'
  }
  
  Package['nrpe'] -> Augeas <| |>

}
