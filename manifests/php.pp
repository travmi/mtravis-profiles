class profile::php {
  
  require ::repo
  
  class { '::php':
    version => '5.5.18-1.w6',
    service => 'httpd',
  }

  php::module { "cli":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }
  
  php::module { "common":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }

  php::module { "devel":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }
  
  php::module { "gd":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }  

  php::module { "intl":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }
  
  php::module { "mbstring":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }
  
  php::module { "mcrypt":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }
  
  php::module { "mysql":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }
  
  php::module { "opcache":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }
  
  php::module { "pdo":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }
 
# By default pear is installed with php55w in this module  
#  php::module { "pear":
#    version => '5.5.18-1.w6',
#    module_prefix => "php55w-",
#  }
  
  php::module { "pgsql":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }
  
  php::module { "soap":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }
  
  php::module { "xml":
    version => '5.5.18-1.w6',
    module_prefix => "php55w-",
  }
  
  php::pecl::module { "redis":
    use_package     => "no",
    verbose         => true,
  }

  php::pecl::module { "ssh2-beta":
    use_package     => "no",
    preferred_state => "beta",
    verbose         => true,
    require         => Package["libssh2", "libssh2-devel"]
  }
  
  php::augeas {
    'php-expose_php':
      entry => 'PHP/expose_php',
      value => 'Off';
    'php-max_execution_time':
      entry => 'PHP/max_execution_time',
      value => '120';
    'php-max_input_time':
      entry => 'PHP/max_input_time',
      value => '120';
    'php-post_max_size':
      entry => 'PHP/post_max_size',
      value => '256M';
    'php-upload_max_filesize':
      entry => 'PHP/upload_max_filesize',
      value => '256M';     
  }

  # There are major problems with the PHP module when install ssh2-beta via PECL.
  # It will install it but does not save an ini file for it. This ini file is 
  # required in order for PHP to load the module.
  # This can be verfied with 'php -m' and 'pecl list'
  file { '/etc/php.d/ssh2-beta.ini':
    ensure  => 'present',
    content => "extension=ssh2.so"
  }

  logrotate::file { 'php_errors':
    log        => [ '/var/log/php_errors' ],
    options    => [ 'missingok',
                    'daily',
                    'notifemty',
                    'rotate 4' ],
    postrotate => [ '' ],          
  }
  
}