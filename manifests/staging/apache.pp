# Class profile::staging::apache 
class profile::staging::apache {
  
  require profile::staging::mounts
  include ::apache
  
#  apache::vhost { 'aabb.staging.coursestage.com':
#    url                  => 'aabb.staging.coursestage.com',
#    server_name          => 'aabb.staging.coursestage.com',
#    client_folder        => aabb,
#    logstash_log_forward => true,
#    error_doc            => false,
#    options              => true,
#    server_alias         => [ 'aabb.staging' ],
#    error_log            => aabb_staging_error_log,
#    access_log           => aabb_staging_access_log
#  }

#  apache::vhost { 'acns.staging.coursestage.com':
#    url                  => 'acns.staging.coursestage.com',
#    server_name          => 'acns.staging.coursestage.com',
#    client_folder        => acns,
#    logstash_log_forward => true,
#    error_doc            => false,
#    options              => true,
#    server_alias         => [ 'acns.staging' ],
#    error_log            => acns_staging_error_log,
#    access_log           => acns_staging_access_log
#  }

#  apache::vhost { 'aiim.staging.coursestage.com':
#    url                  => 'aiim.staging.coursestage.com',
#    server_name          => 'aiim.staging.coursestage.com',
#    client_folder        => aiim,
#    logstash_log_forward => true,
#    error_doc            => false,
#    options              => true,
#    server_alias         => [ 'aiim.staging' ],
#    error_log            => aiim_staging_error_log,
#    access_log           => aiim_staging_access_log
#  }

#  apache::vhost { 'broadway-sandbox.staging.coursestage.com':
#    url                  => 'broadway-sandbox.staging.coursestage.com',
#    server_name          => 'broadway-sandbox.staging.coursestage.com',
#    client_folder        => sandbox,
#    logstash_log_forward => true,
#    error_doc            => false,
#    options              => true,
#    server_alias         => [ 'broadway-sandbox.staging' ],
#    error_log            => broadway-sandbox_staging_error_log,
#    access_log           => broadway-sandbox_staging_access_log
#  }
 
#  apache::vhost { 'ccim-upgrade2.staging.coursestage.com':
#    url                  => 'ccim-upgrade2.staging.coursestage.com',
#    server_name          => 'ccim-upgrade2.staging.coursestage.com',
#    client_folder        => ccim,
#    logstash_log_forward => true,
#    error_doc            => false,
#    options              => true,
#    server_alias         => [ 'ccim-upgrade2.staging' ],
#    error_log            => ccim-upgrade2_staging_error_log,
#    access_log           => ccim-upgrade2_staging_access_log
#  }

#  apache::vhost { 'ccim-upgrade.staging.coursestage.com':
#    url                  => 'ccim-upgrade.staging.coursestage.com',
#    server_name          => 'ccim-upgrade.staging.coursestage.com',
#    client_folder        => ccim,
#    logstash_log_forward => true,
#    error_doc            => false,
#    options              => true,
#    server_alias         => [ 'ccim-upgrade.staging' ],
#    error_log            => ccim-upgrade_staging_error_log,
#    access_log           => ccim-upgrade_staging_access_log
#  }
  
#  apache::vhost { 'hierarchy-testing.staging.coursestage.com':
#    url                  => 'hierarchy-testing.staging.coursestage.com',
#    server_name          => 'hierarchy-testing.staging.coursestage.com',
#    client_folder        => hierarchy,
#    logstash_log_forward => true,
#    error_doc            => false,
#    options              => true,
#    server_alias         => [ 'hierarchy.staging' ],
#    error_log            => hierarchy-testing_staging_error_log,
#    access_log           => hierarchy-testing_staging_access_log
#  }

  apache::vhost { 'naela.staging.coursestage.com':
    url                  => 'naela.staging.coursestage.com',
    server_name          => 'naela.staging.coursestage.com',
    client_folder        => naela,
    logstash_log_forward => true,
    error_doc            => false,
    options              => true,
    server_alias         => [ 'naela.staging' ],
    error_log            => naela_staging_error_log,
    access_log           => naela_staging_access_log
  }

#  apache::vhost { 'niche-broadway-upgrade.staging.coursestage.com':
#    url                  => 'niche-broadway-upgrade.staging.coursestage.com',
#    server_name          => 'niche-broadway-upgrade.staging.coursestage.com',
#    client_folder        => niche,
#    logstash_log_forward => true,
#    error_doc            => false,
#    options              => true,
#    server_alias         => [ 'niche-broadway.staging' ],
#    error_log            => niche-broadway-upgrade_staging_error_log,
#    access_log           => niche-broadway-upgrade_staging_access_log
#  }

#  apache::vhost { 'niche-upgrade.staging.coursestage.com':
#    url                  => 'niche-upgrade.staging.coursestage.com',
#    server_name          => 'niche-upgrade.staging.coursestage.com',
#    client_folder        => niche,
#    logstash_log_forward => true,
#    error_doc            => false,
#    options              => true,
#    server_alias         => [ 'niche-upgrade.staging' ],
#    error_log            => niche_upgrade_staging_error_log,
#    access_log           => niche_upgrade_staging_access_log
#  }

}