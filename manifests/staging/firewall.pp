class profile::staging::firewall {
  
  firewall { '100 allow http acess':
    port   => 80,
    proto  => tcp,
    action => accept,
  }
  
}