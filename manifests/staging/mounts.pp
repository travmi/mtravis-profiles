class profile::staging::mounts {

  file { '/mnt/nfs':
    ensure => 'directory',
  }

  mounttab { '/mnt/nfs':
    ensure   => present,
    device   => 'nfs-staging.wcw.local:/exports/staging',
    fstype   => 'nfs',
    options  => [ 'intr',
                  'soft',
                  'async',
                  'tcp',
                  'nfsvers=3',
                  'noatime',
                  '_netdev' ],
    dump     => '0',
    pass     => '0',
    provider => augeas,
  }

  mounttab { '/var/www':
    ensure   => present,
    device   => '/mnt/nfs/home',
    fstype   => 'none',
    options  => [ 'bind',
                  '_netdev' ],
    dump     => '0',
    pass     => '0',
    provider => augeas,
    before   => Service['httpd', 'nginx']
  }

  Package['ruby-augeas'] -> Augeas <| |>

}