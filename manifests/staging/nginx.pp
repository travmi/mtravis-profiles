class profile::staging::nginx {
  
  include ::nginx
  
#  nginx::vhost { 'aabb.staging.coursestage.com':
#    client_folder => aabb,
#    access_log    => aabb_staging_access_log,
#    error_log     => aabb_staging_error_log,
#    url           => 'aabb.staging.coursestage.com'
#  }

#  nginx::vhost { 'acns.staging.coursestage.com':
#    client_folder => acns,
#    access_log    => acns__staging_access_log,
#    error_log     => acns_staging_error_log,
#    url           => 'acns.staging.coursestage.com'
#  }

#  nginx::vhost { 'aiim.staging.coursestage.com':
#    client_folder => aiim,
#    access_log    => aiim__staging_access_log,
#    error_log     => aiim_staging_error_log,
#    url           => 'aiim.staging.coursestage.com'
#  }

#  nginx::vhost { 'arrs-test.staging.coursestage.com':
#    client_folder => arrs,
#    access_log    => arrs-test_staging_access_log,
#    error_log     => arrs-test_staging_error_log,
#    url           => 'arrs-test.staging.coursestage.com'
#  }
  
#  nginx::vhost { 'broadway-sandbox.staging.coursestage.com':
#    client_folder => sandbox,
#    access_log    => broadway-sandbox_staging_access_log,
#    error_log     => broadway-sandbox_staging_error_log,
#    url           => 'broadway-sandbox.staging.coursestage.com'
#  }

#  nginx::vhost { 'ccim-upgrade2.staging.coursestage.com':
#    client_folder => ccim,
#    access_log    => ccim-upgrade2__staging_access_log,
#    error_log     => ccim-upgrade2_staging_error_log,
#    url           => 'ccim-upgrade2.staging.coursestage.com'
#  }

#  nginx::vhost { 'ccim-upgrade.staging.coursestage.com':
#    client_folder => ccim,
#    access_log    => ccim-upgrade_staging_access_log,
#    error_log     => ccim-upgrade_staging_error_log,
#    url           => 'ccim-upgrade.staging.coursestage.com'
#  }

#  nginx::vhost { 'hierarchy-testing.staging.coursestage.com':
#    client_folder => hierarchy,
#    access_log    => hierarchy-testing_staging_access_log,
#    error_log     => hierarchy-testing_staging_error_log,
#    url           => 'hierarchy-testing.staging.coursestage.com'
#  }

  nginx::vhost { 'naela.staging.coursestage.com':
    client_folder => naela,
    access_log    => naela_staging_access_log,
    error_log     => naela_staging_error_log,
    url           => 'naela.staging.coursestage.com'
  }

#  nginx::vhost { 'niche-broadway-upgrade.staging.coursestage.com':
#    client_folder => niche,
#    access_log    => niche-broadway-upgrade_staging_access_log,
#    error_log     => niche-broadway-upgrade_staging_error_log,
#    url           => 'niche-broadway-upgrade.staging.coursestage.com'
#  }
  
}